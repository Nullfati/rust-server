PLUGIN.Title        = "Helptext"
PLUGIN.Description  = "Hooks into plugins to send helptext"
PLUGIN.Author       = "#Domestos"
PLUGIN.Version      = V(1, 4, 0)
PLUGIN.HasConfig    = true
PLUGIN.ResourceID   = 676

function PLUGIN:Init()
    command.AddChatCommand("help", self.Object, "cmdHelp")
    self:LoadDefaultConfig()
end

function PLUGIN:LoadDefaultConfig()
    self.Config.Settings = self.Config.Settings or {}
    self.Config.Settings.UseCustomHelpText = self.Config.Settings.UseCustomHelpText or "true"
    self.Config.Settings.AllowHelpTextFromOtherPlugins = self.Config.Settings.AllowHelpTextFromOtherPlugins or "false"
    self.Config.CustomHelpText = self.Config.CustomHelpText or {
       "Список доступных команд:",
	   "/clan - Получить подробную информацию о создании и редактировании клана.",
       "/prod - Узнайте владельца дома, введя это у стен дома.",
	   "/kit - Введите для того чтобы узнать информацию о стартовых наборах для новичков.",
	   "/ipanel - Введите для того чтобы узнать информацию о панели снизу.",
	   "/online - Введите для того чтобы узнать сколько игроков на сервере.",
	   "/stats - Введите для того чтобы узнать информацию об уровнях добычи ресурсов.",
	   "/pm nickname - Введите для того чтобы отправить личное сообщение игроку. ",
	   "/tpr nickname - Введите для того чтобы отправить запрос телепортации игроку."
    }
    self:SaveConfig()
end

function PLUGIN:cmdHelp(player)
    if not player then return end
    if self.Config.Settings.UseCustomHelpText == "true" then
        for _, helptext in pairs(self.Config.CustomHelpText) do
            rust.SendChatMessage(player, helptext)
        end
    end
    if self.Config.Settings.AllowHelpTextFromOtherPlugins == "true" then
        plugins.CallHook("SendHelpText", util.TableToArray({player}))
    end
end